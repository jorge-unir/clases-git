class ExperimentosController < ApplicationController
  before_action :set_experimento, only: [:show, :edit, :update, :destroy]

  # GET /experimentos
  # GET /experimentos.json
  def index
    @experimentos = Experimento.all
  end

  # GET /experimentos/1
  # GET /experimentos/1.json
  def show
  end

  # GET /experimentos/new
  def new
    @experimento = Experimento.new
  end

  # GET /experimentos/1/edit
  def edit
  end

  # POST /experimentos
  # POST /experimentos.json
  def create
    @experimento = Experimento.new(experimento_params)

    respond_to do |format|
      if @experimento.save
        format.html { redirect_to @experimento, notice: 'Experimento was successfully created.' }
        format.json { render :show, status: :created, location: @experimento }
      else
        format.html { render :new }
        format.json { render json: @experimento.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /experimentos/1
  # PATCH/PUT /experimentos/1.json
  def update
    respond_to do |format|
      if @experimento.update(experimento_params)
        format.html { redirect_to @experimento, notice: 'Experimento was successfully updated.' }
        format.json { render :show, status: :ok, location: @experimento }
      else
        format.html { render :edit }
        format.json { render json: @experimento.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /experimentos/1
  # DELETE /experimentos/1.json
  def destroy
    @experimento.destroy
    respond_to do |format|
      format.html { redirect_to experimentos_url, notice: 'Experimento was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_experimento
      @experimento = Experimento.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def experimento_params
      params.require(:experimento).permit(:apu)
    end
end

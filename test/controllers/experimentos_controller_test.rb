require 'test_helper'

class ExperimentosControllerTest < ActionController::TestCase
  setup do
    @experimento = experimentos(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:experimentos)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create experimento" do
    assert_difference('Experimento.count') do
      post :create, experimento: { apu: @experimento.apu }
    end

    assert_redirected_to experimento_path(assigns(:experimento))
  end

  test "should show experimento" do
    get :show, id: @experimento
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @experimento
    assert_response :success
  end

  test "should update experimento" do
    patch :update, id: @experimento, experimento: { apu: @experimento.apu }
    assert_redirected_to experimento_path(assigns(:experimento))
  end

  test "should destroy experimento" do
    assert_difference('Experimento.count', -1) do
      delete :destroy, id: @experimento
    end

    assert_redirected_to experimentos_path
  end
end

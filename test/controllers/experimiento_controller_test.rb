require 'test_helper'

class ExperimientoControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get formula" do
    get :formula
    assert_response :success
  end

end
